﻿// ReSharper disable UnusedMember.Global

namespace MQ2DotNet.MQ2API.DataTypes
{
    public class PluginType : MQ2DataType
    {
        internal PluginType(MQ2TypeVar typeVar) : base(typeVar)
        {
        }

        /// <summary>
        /// Plugin's name e.g. mq2cast
        /// </summary>
        public string Name => GetMember<StringType>("Name");

        /// <summary>
        /// Plugin's version as exported by the PLUGIN_VERSION macro
        /// </summary>
        public float? Version => GetMember<FloatType>("Version");
    }
}